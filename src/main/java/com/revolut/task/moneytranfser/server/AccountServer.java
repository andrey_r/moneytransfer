package com.revolut.task.moneytranfser.server;

import com.revolut.task.moneytranfser.controller.AccountController;
import com.revolut.task.moneytranfser.handler.RequestHandler;
import com.revolut.task.moneytranfser.service.AccountService;
import com.sun.net.httpserver.HttpServer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.net.InetSocketAddress;

/**
 * Server which provides REST API for accounts and transfers.
 */
public class AccountServer {
    private static final Logger LOGGER = LoggerFactory.getLogger(AccountServer.class);
    private int port;

    private HttpServer server;

    /**
     * Creates server.
     *
     * @param port server port
     * @throws IOException thrown when http server was not created
     */
    public AccountServer(int port) throws IOException {
        this.port = port;
        server = HttpServer.create();
        server.bind(new InetSocketAddress(port), 0);
        server.createContext("/accounts", new RequestHandler(new AccountController(new AccountService())));
    }

    /**
     * Start the server.
     */
    public void start() {
        server.start();
        LOGGER.info("Account server started on port {}", port);
    }

    /**
     * Stop the server.
     * @param stopDelay the maximum time in seconds to wait until exchanges have finished
     */
    public void stop(int stopDelay) {
        server.stop(stopDelay);
        LOGGER.info("Account server stopped", port);
    }
}
