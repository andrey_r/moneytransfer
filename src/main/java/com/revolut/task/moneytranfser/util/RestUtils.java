package com.revolut.task.moneytranfser.util;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

/**
 * Utility class for REST handling.
 */
public class RestUtils {
    private static final Logger LOGGER = LoggerFactory.getLogger(RestUtils.class);

    private RestUtils() {

    }

    /**
     * Parses URI extracting arguments in accordance to passed pattern.
     *
     * @param uri     uri to parse
     * @param pattern uri pattern
     * @return list of arguments
     */
    public static List<String> parseUri(String uri, Pattern pattern) {
        Matcher matcher = pattern.matcher(uri);
        List<String> arguments = new ArrayList<>();
        if (matcher.find()) {
            for (int i = 1; i <= matcher.groupCount(); i++) {
                arguments.add(matcher.group(i));
            }
        }
        return arguments;
    }

    /**
     * Maps an object to json string.
     *
     * @param obj object to map
     * @return json
     */
    public static String mapToJson(Object obj) {
        ObjectMapper mapper = new ObjectMapper();
        mapper.setSerializationInclusion(JsonInclude.Include.NON_NULL);
        try {
            return mapper.writeValueAsString(obj);
        } catch (JsonProcessingException ex) {
            LOGGER.error("was not able to map response to JSON", ex);
        }
        return "";
    }

    /**
     * Maps from string json to object.
     *
     * @param json   string json
     * @param target target class
     * @param <T>    type of target
     * @return json object
     * @throws JsonProcessingException thrown when string can't be parsed to passed class
     */
    public static <T> T mapFromJson(String json, Class<T> target) throws JsonProcessingException {
        ObjectMapper mapper = new ObjectMapper();
        return mapper.readValue(json, target);
    }

    /**
     * Read all data from input stream, e.g. to json string.
     *
     * @param is input stream
     * @return string
     */
    public static String readLines(InputStream is) {
        return new BufferedReader(new InputStreamReader(is)).lines().collect(Collectors.joining());
    }
}
