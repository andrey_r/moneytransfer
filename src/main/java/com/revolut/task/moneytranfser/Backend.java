package com.revolut.task.moneytranfser;

import com.revolut.task.moneytranfser.server.AccountServer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;

public class Backend {
    private static final Logger LOGGER = LoggerFactory.getLogger(Backend.class);

    /**
     * Entry point.
     *
     * @param argc first argument is a server port
     */
    public static void main(String[] args) {
        LOGGER.info("Starting account server");
        try {
            int port = 9080;
            if (args.length > 0) {
                try {
                    port = Integer.valueOf(args[0]);
                } catch (NumberFormatException ex) {
                    LOGGER.warn("Can't parse " + args[0] + ". The server will be run on port " + port);
                }
            }
            AccountServer server = new AccountServer(port);
            Runtime.getRuntime().addShutdownHook(new Thread(() -> {
                server.stop(5);
            }));
            server.start();
        } catch (IOException ex) {
            LOGGER.error("Can't start server", ex);
            return;
        }
    }
}
