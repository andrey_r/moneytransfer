package com.revolut.task.moneytranfser.model.exception;

/**
 * Indicates that target account is not found.
 */
public class TargetAccountNotFoundException extends Exception {
    public TargetAccountNotFoundException(String message) {
        super(message);
    }
}
