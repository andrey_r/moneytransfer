package com.revolut.task.moneytranfser.model;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.math.BigDecimal;

/**
 * Account details.
 */
public class Account {
    // read only field
    // never changed after creation
    private long id;
    /**
     * Account balance.
     */
    private BigDecimal balance;

    @JsonCreator
    public Account(@JsonProperty("id") long id) {
        this.id = id;
    }

    public long getId() {
        return id;
    }

    public void setBalance(BigDecimal balance) {
        this.balance = balance;
    }

    public BigDecimal getBalance() {
        return balance;
    }

    /**
     * Update account.
     *
     * @param details account details to update.
     */
    public void updateAccount(Account details) {
        if (details.balance != null) {
            this.balance = details.balance;
        }
    }

    @Override
    public String toString() {
        return "Account{" +
                "id=" + id +
                ", balance=" + balance +
                '}';
    }
}
