package com.revolut.task.moneytranfser.model;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.math.BigDecimal;

/**
 * Transfer details.
 */
public class Transfer {
    /**
     * Target account to transfer money to.
     */
    private Account targetAccount;
    /**
     * Money to transfer.
     */
    private BigDecimal moneyToTransfer;

    @JsonCreator
    public Transfer(@JsonProperty("targetAccount") Account targetAccount,
                    @JsonProperty("moneyToTransfer") BigDecimal moneyToTransfer) {
        this.targetAccount = targetAccount;
        this.moneyToTransfer = moneyToTransfer;
    }

    public Account getTargetAccount() {
        return targetAccount;
    }

    public BigDecimal getMoneyToTransfer() {
        return moneyToTransfer;
    }
}
