package com.revolut.task.moneytranfser.model.exception;

/**
 * Inidicates that a user provided invalid input data.
 */
public class InvalidInputException extends Exception {
    public InvalidInputException(String message) {
        super(message);
    }
}
