package com.revolut.task.moneytranfser.model.exception;

/**
 * Inidicates that a resource is not found.
 */
public class ResourceNotFoundException extends Exception {
    public ResourceNotFoundException(String message) {
        super(message);
    }
}

