package com.revolut.task.moneytranfser.model.exception;

/**
 * Indicates that an account has not enough data.
 */
public class NotEnoughMoneyException extends Exception {
    public NotEnoughMoneyException(String message) {
        super(message);
    }
}
