package com.revolut.task.moneytranfser.service;

import com.revolut.task.moneytranfser.model.Account;
import com.revolut.task.moneytranfser.model.Transfer;
import com.revolut.task.moneytranfser.model.exception.ResourceNotFoundException;
import com.revolut.task.moneytranfser.model.exception.NotEnoughMoneyException;
import com.revolut.task.moneytranfser.model.exception.TargetAccountNotFoundException;

import java.math.BigDecimal;
import java.util.*;
import java.util.concurrent.ConcurrentHashMap;

public class AccountService {
    // accounts storage
    private Map<Long, Account> accounts = new ConcurrentHashMap<>();

    /**
     * Creates new account with zero balance.
     * @return created account
     */
    public Account createAccount() {
        return createAccount(null);
    }

    /**
     * Creates new account with provided details.
     * @param accountDetails account details.
     * @return created account
     */
    public Account createAccount(Account accountDetails) {
        long id = accounts.size() + 1;
        Account account = new Account(id);
        if (accountDetails == null) {
            account.setBalance(BigDecimal.ZERO);
        } else {
            account.updateAccount(accountDetails);
        }
        accounts.put(id, account);
        return account;
    }

    /**
     * Gets list of existing accounts ordered by id.
     * @return list of accounts
     */
    public Collection<Account> getAccounts() {
        List<Account> values = new ArrayList<Account>(accounts.values());
        Collections.sort(values, Comparator.comparingLong(Account::getId));
        return values;
    }

    /**
     * Gets account.
     * @param account account specification (only id is used)
     * @return account details
     * @throws ResourceNotFoundException if account is not found
     */
    public Account getAccount(Account account) throws ResourceNotFoundException {
        checkAccountExists(account);
        return accounts.get(account.getId());
    }

    /**
     * Transfer money between accounts.
     * @param sourceAccount source account
     * @param transfer transfer details (target account, money to transfer)
     * @throws ResourceNotFoundException if source account is not found
     * @throws TargetAccountNotFoundException if target account is not found
     * @throws NotEnoughMoneyException if source account doesn't have enough money
     */
    public void transfer(Account sourceAccount, Transfer transfer) throws ResourceNotFoundException,
            TargetAccountNotFoundException, NotEnoughMoneyException {
        checkAccountExists(sourceAccount);
        if (transfer == null || transfer.getTargetAccount() == null || transfer.getMoneyToTransfer() == null) {
            throw new IllegalArgumentException(("parameters of transfer are not set"));
        }
        if (!accounts.containsKey(transfer.getTargetAccount().getId())) {
            throw new TargetAccountNotFoundException(String.format("account %s doesn't exist", transfer.getTargetAccount().getId()));
        }
        BigDecimal moneyToTransfer = transfer.getMoneyToTransfer();
        Account source = accounts.get(sourceAccount.getId());
        synchronized (source) {
            BigDecimal sourceBalanceAfter = source.getBalance().subtract(moneyToTransfer);
            if (sourceBalanceAfter.compareTo(BigDecimal.ZERO) < 0) {
                throw new NotEnoughMoneyException(String.format("account %s doesn't have enough money to transfer",
                        sourceAccount.getId()));
            }
            source.setBalance(sourceBalanceAfter);
        }
        Account target = accounts.get(transfer.getTargetAccount().getId());
        synchronized (target) {
            target.setBalance(target.getBalance().add(moneyToTransfer));
        }
    }

    /**
     * Checks whether account exists.
     * @param account account to check
     * @throws ResourceNotFoundException thrown if account doesn't exist
     */
    private void checkAccountExists(Account account) throws ResourceNotFoundException {
        if (!accounts.containsKey(account.getId())) {
            throw new ResourceNotFoundException(String.format("account %s doesn't exist", account.getId()));
        }
    }

}
