package com.revolut.task.moneytranfser.handler;

import com.revolut.task.moneytranfser.controller.AccountController;
import com.revolut.task.moneytranfser.controller.ThrowingException;
import com.revolut.task.moneytranfser.model.exception.ResourceNotFoundException;
import com.revolut.task.moneytranfser.util.RestUtils;
import com.sun.net.httpserver.HttpExchange;
import com.sun.net.httpserver.HttpHandler;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.io.OutputStream;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.regex.Pattern;

/**
 * Common handler for http request for account context.
 */
public class RequestHandler implements HttpHandler {

    private static final Logger LOGGER = LoggerFactory.getLogger(RequestHandler.class);

    private Map<Pattern, ThrowingException<String[], Object>> routing;

    /**
     * Creates handler instance.
     * @param controller account controller to redirect requests
     */
    public RequestHandler(AccountController controller) {
        routing = controller.getRouting();
    }

    /**
     * Handles http requests, does basic checks and transformations and sends request to controller.
     * @param httpExchange request/response data
     */
    @Override
    public void handle(HttpExchange httpExchange) throws IOException {
        String method = httpExchange.getRequestMethod();
        String uriWithMethod = String.format("%s %s", method, httpExchange.getRequestURI().toString());
        // check whether uri matches to uri pattern from controller
        Optional<Pattern> routingPattern = routing.keySet().stream()
                .filter(pattern -> pattern.asPredicate().test(uriWithMethod)).findAny();
        if (routingPattern.isPresent()) {
            Pattern pattern = routingPattern.get();
            // extract arguments from url
            List<String> arguments = RestUtils.parseUri(uriWithMethod, pattern);
            if ("POST".equals(method)) {
                // add request body to arguments list for 'POST' requests
                arguments.add(RestUtils.readLines(httpExchange.getRequestBody()));
            }
            byte[] response = null;
            int status;
            try {
                response = RestUtils.mapToJson(routing.get(pattern)
                        .apply(arguments.toArray(new String[arguments.size()]))).getBytes();
                status = 200;
            } catch (ResourceNotFoundException ex) {
                status = 404;
                response = new byte[0];
            } catch (Exception ex) {
                // when errors occur, return status 501 with explanation
                status = 500;
                Map<String, String> result = new HashMap<>();
                result.put("message", ex.getMessage());
                response = RestUtils.mapToJson(result).getBytes();
            }
            httpExchange.getResponseHeaders().set("Content-Type", "application/json");
            httpExchange.sendResponseHeaders(status, response.length);
            // add response body to http response
            try (OutputStream os = httpExchange.getResponseBody()) {
                os.write(response);
            }
        } else {
            // return 4040 when uri doesn't match uri pattern
            httpExchange.sendResponseHeaders(404, 0);
        }
        httpExchange.close();
    }
}
