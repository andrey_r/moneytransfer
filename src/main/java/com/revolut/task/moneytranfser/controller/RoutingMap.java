package com.revolut.task.moneytranfser.controller;

import java.util.Map;
import java.util.regex.Pattern;

/**
 * An interface for classes, providing routing information, e.g. controllers.
 */
public interface RoutingMap {
    /**
     * Get routing data.
     * @return routing data
     */
    Map<Pattern, ThrowingException<String[], Object>> getRouting();
}
