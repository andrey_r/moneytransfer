package com.revolut.task.moneytranfser.controller;

/**
 * An interace for controller's methods.
 */
@FunctionalInterface
public interface ThrowingException<T, V> {
    public V apply(T arg) throws Exception;
}
