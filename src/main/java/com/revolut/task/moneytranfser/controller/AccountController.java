package com.revolut.task.moneytranfser.controller;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.revolut.task.moneytranfser.model.Account;
import com.revolut.task.moneytranfser.model.Transfer;
import com.revolut.task.moneytranfser.model.exception.InvalidInputException;
import com.revolut.task.moneytranfser.model.exception.NotEnoughMoneyException;
import com.revolut.task.moneytranfser.model.exception.ResourceNotFoundException;
import com.revolut.task.moneytranfser.model.exception.TargetAccountNotFoundException;
import com.revolut.task.moneytranfser.service.AccountService;
import com.revolut.task.moneytranfser.util.RestUtils;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;
import java.util.regex.Pattern;

/**
 * A controller for redirecting rest requests.
 */
public class AccountController implements RoutingMap {
    private Map<Pattern, ThrowingException<String[], Object>> routing = new HashMap<>();
    private AccountService accountService;

    public AccountController(AccountService accountService) {
        this.accountService = accountService;
        // create new account POST /accounts/
        // account details are passed in request body
        routing.put(Pattern.compile("^POST /accounts/$"), arguments -> createAccount(arguments[0]));
        // get all accounts GET /accounts/
        routing.put(Pattern.compile("^GET /accounts/$"), arguments -> getAccounts());
        // get account by id GET /account/<id>
        routing.put(Pattern.compile("^GET /accounts/(\\d+)$"), arguments -> getAccount(arguments[0]));
        // make (create) transfer from account POST /account/<id>/transfers/
        // transfer details are passed in request body
        routing.put(Pattern.compile("^POST /accounts/(\\d+)/transfers/$"),
                arguments -> transfer(arguments[0], arguments[1]));
    }

    /**
     * Crates new account.
     * @param accountJson optional account details
     * @return created account
     * @throws InvalidInputException if provided details can't be parsed
     */
    public Account createAccount(String accountJson) throws InvalidInputException {
        if (accountJson == null || accountJson.isEmpty()) {
            return accountService.createAccount();
        } else {
            Account account = null;
            try {
                account = RestUtils.mapFromJson(accountJson, Account.class);
            } catch (JsonProcessingException e) {
                throw new InvalidInputException("can't parse account details");
            }
            return accountService.createAccount(account);
        }
    }

    /**
     * Gets list of existing accounts.
     * @return list of accounts
     */
    public Collection<Account> getAccounts() {
        return accountService.getAccounts();
    }

    /**
     * Gets account by id.
     * @param accountId account id
     * @return account
     * @throws ResourceNotFoundException if account doesn't exist
     */
    public Account getAccount(String accountId) throws ResourceNotFoundException {
        return accountService.getAccount(new Account(Long.valueOf(accountId)));
    }

    /**
     * Makes money transfer.
     * @param accountId source account id
     * @param transferJson transfer details (target account and money to transfer)
     * @return transfer details
     * @throws InvalidInputException transfer details can't be parser
     * @throws ResourceNotFoundException source account doesn't exist
     * @throws NotEnoughMoneyException source account doesn't have enough money to transfer
     * @throws TargetAccountNotFoundException target account doesn't exist
     */
    public Transfer transfer(String accountId, String transferJson) throws InvalidInputException, ResourceNotFoundException, NotEnoughMoneyException, TargetAccountNotFoundException {
        Transfer transfer = null;
        try {
            transfer = RestUtils.mapFromJson(transferJson, Transfer.class);
        } catch (JsonProcessingException e) {
            throw new InvalidInputException("can't parse transfer details");
        }
        accountService.transfer(new Account(Long.valueOf(accountId)), transfer);
        return transfer;
    }

    @Override
    public Map<Pattern, ThrowingException<String[], Object>> getRouting() {
        return routing;
    }
}
