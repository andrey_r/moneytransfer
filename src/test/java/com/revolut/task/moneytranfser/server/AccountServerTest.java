package com.revolut.task.moneytranfser.server;

import com.revolut.task.moneytranfser.util.SocketUtils;
import org.apache.http.HttpResponse;
import org.apache.http.HttpStatus;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.util.EntityUtils;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.io.IOException;
import java.math.BigDecimal;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

/**
 * Integration tests for REST API.
 */
public class AccountServerTest {

    private class Response {
        private int status;
        private String body;

        public Response(int status, String body) {
            this.status = status;
            this.body = body;
        }
    }

    private AccountServer server;
    private String baseUrl;
    private HttpClient client;

    @BeforeEach
    public void startServer() throws IOException {
        int port = SocketUtils.findFreePort();
        server = new AccountServer(port);
        server.start();
        baseUrl = "http://127.0.0.1:" + port;
        client = HttpClients.createDefault();
    }

    @AfterEach
    public void stopServer() {
        server.stop(0);
    }

    @Test
    public void testServerReturnsNotFoundOnUknownResource() throws IOException {
        HttpGet request = new HttpGet(baseUrl);
        HttpResponse response = client.execute(request);
        assertEquals(HttpStatus.SC_NOT_FOUND, response.getStatusLine().getStatusCode());
    }

    @Test
    public void testAccountCreation() throws IOException {
        Response response = createDeaultAccount();
        assertEquals(HttpStatus.SC_OK, response.status);
        assertNotNull(response.body);
        assertEquals("{\"id\":1,\"balance\":0}", response.body);
    }

    @Test
    public void testGetAccount() throws IOException {
        // create account
        createDeaultAccount();
        // read account details
        HttpGet getRequest = new HttpGet(baseUrl + "/accounts/1");
        HttpResponse getResponse = client.execute(getRequest);
        assertEquals(HttpStatus.SC_OK, getResponse.getStatusLine().getStatusCode());
        String accounts = EntityUtils.toString(getResponse.getEntity());
        assertNotNull(accounts);
        assertEquals("{\"id\":1,\"balance\":0}", accounts);
    }

    @Test
    public void testGetAccounts() throws IOException {
        // create account
        createDeaultAccount();
        // read accounts details
        HttpGet getRequest = new HttpGet(baseUrl + "/accounts/");
        HttpResponse getResponse = client.execute(getRequest);
        assertEquals(HttpStatus.SC_OK, getResponse.getStatusLine().getStatusCode());
        String accounts = EntityUtils.toString(getResponse.getEntity());
        assertNotNull(accounts);
        assertEquals("[{\"id\":1,\"balance\":0}]", accounts);
    }

    @Test
    public void testAccountWithMoneyCreation() throws IOException {
        Response response = createAccountWithBalance(BigDecimal.valueOf(100500));
        assertEquals(HttpStatus.SC_OK, response.status);
        assertNotNull(response.body);
        assertEquals("{\"id\":1,\"balance\":100500}", response.body);
    }

    @Test
    public void testAccountWithBigMoneyCreation() throws IOException {
        Response response = createAccountWithBalance(BigDecimal.valueOf(12345e100).subtract(BigDecimal.valueOf(5)));
        assertEquals(HttpStatus.SC_OK, response.status);
        assertNotNull(response.body);
        assertEquals("{\"id\":1,\"balance\":123449999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999995}", response.body);
    }

    @Test
    public void testGetAccountWithMoney() throws IOException {
        // create account with balance
        createAccountWithBalance(BigDecimal.valueOf(100500));
        // read accounts details
        HttpGet getRequest = new HttpGet(baseUrl + "/accounts/");
        HttpResponse getResponse = client.execute(getRequest);
        assertEquals(HttpStatus.SC_OK, getResponse.getStatusLine().getStatusCode());
        String accounts = EntityUtils.toString(getResponse.getEntity());
        assertNotNull(accounts);
        assertEquals("[{\"id\":1,\"balance\":100500}]", accounts);
    }

    @Test
    public void testTransferOk() throws IOException {
        // create account with balance
        createAccountWithBalance(BigDecimal.valueOf(100500));
        createDeaultAccount();
        Response response = makeTransfer(1, 2, 100);
        assertEquals(HttpStatus.SC_OK, response.status);
    }

    @Test
    public void testTransferOkDetails() throws IOException {
        // create account with balance
        createAccountWithBalance(BigDecimal.valueOf(100500));
        createDeaultAccount();
        makeTransfer(1, 2, 100);
        // read accounts details
        HttpGet getRequest = new HttpGet(baseUrl + "/accounts/");
        HttpResponse getResponse = client.execute(getRequest);
        assertEquals(HttpStatus.SC_OK, getResponse.getStatusLine().getStatusCode());
        String accounts = EntityUtils.toString(getResponse.getEntity());
        assertNotNull(accounts);
        assertEquals("[{\"id\":1,\"balance\":100400},{\"id\":2,\"balance\":100}]", accounts);
    }

    @Test
    public void testTransferNotEnoughMoney() throws IOException {
        // create account with balance
        createAccountWithBalance(BigDecimal.valueOf(50));
        createDeaultAccount();
        Response response = makeTransfer(1, 2, 100);
        assertEquals(HttpStatus.SC_INTERNAL_SERVER_ERROR, response.status);
        assertEquals("{\"message\":\"account 1 doesn't have enough money to transfer\"}", response.body);
    }

    @Test
    public void testTransferNotEnoughMoneyDetails() throws IOException {
        // create account with balance
        createAccountWithBalance(BigDecimal.valueOf(50));
        createDeaultAccount();
        makeTransfer(1, 2, 100);
        // read accounts details
        HttpGet getRequest = new HttpGet(baseUrl + "/accounts/");
        HttpResponse getResponse = client.execute(getRequest);
        assertEquals(HttpStatus.SC_OK, getResponse.getStatusLine().getStatusCode());
        String accounts = EntityUtils.toString(getResponse.getEntity());
        assertNotNull(accounts);
        assertEquals("[{\"id\":1,\"balance\":50},{\"id\":2,\"balance\":0}]", accounts);
    }

    @Test
    public void testTransferNotExistingSourceAccount() throws IOException {
        Response response = makeTransfer(1, 3, 100);
        assertEquals(HttpStatus.SC_NOT_FOUND, response.status);
    }

    @Test
    public void testTransferNotExistingTargetAccount() throws IOException {
        // create account with balance
        createAccountWithBalance(BigDecimal.valueOf(50));
        Response response = makeTransfer(1, 3, 100);
        assertEquals(HttpStatus.SC_INTERNAL_SERVER_ERROR, response.status);
        assertEquals("{\"message\":\"account 3 doesn't exist\"}", response.body);
    }

    @Test
    public void testTransferWithoutDetails() throws IOException {
        // create account with balance
        createAccountWithBalance(BigDecimal.valueOf(50));
        createDeaultAccount();
        HttpPost request = new HttpPost(baseUrl + "/accounts/1/transfers/");
        HttpResponse response = client.execute(request);
        Response result = new Response(response.getStatusLine().getStatusCode(),
                EntityUtils.toString(response.getEntity()));
        request.releaseConnection();
        assertEquals(HttpStatus.SC_INTERNAL_SERVER_ERROR, result.status);
        assertEquals("{\"message\":\"can't parse transfer details\"}", result.body);
    }


    private Response createDeaultAccount() throws IOException {
        HttpPost request = new HttpPost(baseUrl + "/accounts/");
        HttpResponse response = client.execute(request);
        Response result = new Response(response.getStatusLine().getStatusCode(),
                EntityUtils.toString(response.getEntity()));
        request.releaseConnection();
        return result;
    }

    private Response createAccountWithBalance(BigDecimal money) throws IOException {
        HttpPost request = new HttpPost(baseUrl + "/accounts/");
        request.setEntity(new StringEntity("{\"id\":1,\"balance\":" + money + "}"));
        HttpResponse response = client.execute(request);
        Response result = new Response(response.getStatusLine().getStatusCode(),
                EntityUtils.toString(response.getEntity()));
        request.releaseConnection();
        return result;
    }

    private Response makeTransfer(int sourceId, int targetId, int money) throws IOException {
        HttpPost request = new HttpPost(baseUrl + "/accounts/" + sourceId + "/transfers/");
        request.setEntity(new StringEntity("{\"targetAccount\":{\"id\":" +
                targetId + "},\"moneyToTransfer\":" + money + "}"));
        HttpResponse response = client.execute(request);
        Response result = new Response(response.getStatusLine().getStatusCode(),
                EntityUtils.toString(response.getEntity()));
        request.releaseConnection();
        return result;
    }
}