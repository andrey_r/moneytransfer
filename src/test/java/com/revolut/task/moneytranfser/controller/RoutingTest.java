package com.revolut.task.moneytranfser.controller;

import org.junit.jupiter.api.Test;

import java.util.Map;
import java.util.regex.Pattern;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class RoutingTest {

    @Test
    public void testAcountControllerRouting() {
        assertSyntaxMatches(new AccountController(null));
    }

    private void assertSyntaxMatches(RoutingMap routingMap) {
        Map<Pattern, ThrowingException<String[], Object>> routing = routingMap.getRouting();
        assertNotNull(routing);
        assertFalse(routing.isEmpty());
        final String keyPattern = "^\\^?(POST|GET) .*$";
        routing.forEach((key, value) -> {
            assertTrue(key.pattern().matches(keyPattern), String.format("%s doesn't match to pattern %s", key.pattern(), keyPattern));
        });
    }

}