package com.revolut.task.moneytranfser.model;

import org.junit.jupiter.api.Test;

import java.math.BigDecimal;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class AccountTest {
    @Test
    public void testUpdate() {
        Account account = new Account(1);
        Account details = new Account(100500);
        details.setBalance(BigDecimal.valueOf(200));
        account.updateAccount(details);
        assertEquals(1, account.getId());
        assertEquals(BigDecimal.valueOf(200), account.getBalance());
    }
}