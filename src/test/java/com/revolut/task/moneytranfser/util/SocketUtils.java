package com.revolut.task.moneytranfser.util;

import java.io.IOException;
import java.net.ServerSocket;

public class SocketUtils {
    private SocketUtils() {
    }

    /**
     * Looks for free port.
     *
     * @return free port
     * @throws IOException if an I/O error occurs when opening the socket.
     */
    public static int findFreePort() throws IOException {
        try (
                ServerSocket socket = new ServerSocket(0);
        ) {
            return socket.getLocalPort();

        }
    }
}
