package com.revolut.task.moneytranfser.util;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.revolut.task.moneytranfser.model.Account;
import org.junit.jupiter.api.Test;

import java.math.BigDecimal;
import java.util.Arrays;
import java.util.Collections;
import java.util.regex.Pattern;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertIterableEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

public class RestUtilsTest {
    @Test
    public void testParse() {
        assertIterableEquals(Collections.emptyList(),
                RestUtils.parseUri("", Pattern.compile("")));
        assertIterableEquals(Collections.emptyList(),
                RestUtils.parseUri("/accounts", Pattern.compile("^/accounts/(\\w+)/?$")));
        assertIterableEquals(Arrays.asList("100500"),
                RestUtils.parseUri("/accounts/100500/", Pattern.compile("^/accounts/(\\w+)/?$")));
        assertIterableEquals(Collections.emptyList(),
                RestUtils.parseUri("/accounts/100500/transfers/", Pattern.compile("^/accounts/(\\w+)/?$")));
        assertIterableEquals(Arrays.asList("100500", "200"),
                RestUtils.parseUri("/accounts/100500/transfers/200", Pattern.compile("^/accounts/(\\w+)/transfers/(\\w+)/?$")));
    }

    @Test
    public void testMapToJson() {
        Account account = new Account(500);
        account.setBalance(BigDecimal.valueOf(200));
        String json = RestUtils.mapToJson(account);
        assertEquals("{\"id\":500,\"balance\":200}", json);
    }

    @Test
    public void testMapToJsonIgnoreNull() {
        Account account = new Account(500);
        String json = RestUtils.mapToJson(account);
        assertEquals("{\"id\":500}", json);
    }

    @Test
    public void testFromJson() throws JsonProcessingException {
        Account account = RestUtils.mapFromJson("{\"id\":500,\"balance\":200}", Account.class);
        assertEquals(500, account.getId());
        assertEquals(BigDecimal.valueOf(200), account.getBalance());
    }

    @Test
    public void testFromInvalidJson() {
        assertThrows(JsonProcessingException.class, () -> RestUtils.mapFromJson("json", Account.class));
    }
}