package com.revolut.task.moneytranfser.service;

import com.revolut.task.moneytranfser.model.Account;
import com.revolut.task.moneytranfser.model.Transfer;
import com.revolut.task.moneytranfser.model.exception.ResourceNotFoundException;
import com.revolut.task.moneytranfser.model.exception.NotEnoughMoneyException;
import com.revolut.task.moneytranfser.model.exception.TargetAccountNotFoundException;
import org.junit.jupiter.api.Test;

import java.math.BigDecimal;
import java.util.Collection;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertThrows;

public class AccountServiceTest {
    @Test
    public void testCreation() {
        AccountService service = new AccountService();
        Account account = service.createAccount();
        assertNotNull(account);
        assertEquals(1, account.getId());
        assertEquals(BigDecimal.ZERO, account.getBalance());
    }

    @Test
    public void testGetAccounts() {
        AccountService service = new AccountService();
        service.createAccount();
        Account middle = service.createAccount();
        service.createAccount();
        Collection<Account> accounts = service.getAccounts();
        assertNotNull(accounts);
        assertEquals(3, accounts.size());
        assertEquals(1, accounts.stream().filter(acc -> middle.getId() == acc.getId()).count());
    }

    @Test
    public void testGetAccount() throws ResourceNotFoundException {
        AccountService service = new AccountService();
        service.createAccount();
        Account middle = service.createAccount();
        service.createAccount();
        Account account = service.getAccount(new Account(middle.getId()));
        assertNotNull(account);
        assertEquals(middle.getId(), account.getId());
        assertEquals(BigDecimal.ZERO, account.getBalance());
    }

    @Test
    public void testGetAccountNotFound() throws ResourceNotFoundException {
        AccountService service = new AccountService();
        assertThrows(ResourceNotFoundException.class, () -> service.getAccount(new Account(1)));
    }

    @Test
    public void testCreationWithDetails() throws ResourceNotFoundException {
        AccountService service = new AccountService();
        Account accountDetails = new Account(0);
        accountDetails.setBalance(BigDecimal.valueOf(100));
        Account newAccount = service.createAccount(accountDetails);
        Account account = service.getAccount(new Account(newAccount.getId()));
        assertNotNull(account);
        assertEquals(newAccount.getId(), account.getId());
        assertEquals(BigDecimal.valueOf(100), account.getBalance());
    }

    @Test
    public void testTransferNoTarget() {
        AccountService service = new AccountService();
        Account account = service.createAccount();
        Transfer transfer = new Transfer(new Account(500), BigDecimal.valueOf(100));
        assertThrows(TargetAccountNotFoundException.class, () -> service.transfer(account, transfer));
    }

    @Test
    public void testTransferOk() throws ResourceNotFoundException, TargetAccountNotFoundException, NotEnoughMoneyException {
        AccountService service = new AccountService();
        Account acountDetails = new Account(0);
        acountDetails.setBalance(BigDecimal.valueOf(100500));
        Account sourceAccount = service.createAccount(acountDetails);
        Account targetAccount = service.createAccount();
        Transfer transfer = new Transfer(new Account(targetAccount.getId()), BigDecimal.valueOf(100));
        service.transfer(sourceAccount, transfer);
        Account sourceAccountAfter = service.getAccount(new Account(sourceAccount.getId()));
        Account targetAccountAfter = service.getAccount(new Account(targetAccount.getId()));
        assertEquals(BigDecimal.valueOf(100500 - 100), sourceAccountAfter.getBalance());
        assertEquals(BigDecimal.valueOf(100), targetAccountAfter.getBalance());
    }

    @Test
    public void testTransferNotEnoughMoney() throws ResourceNotFoundException {
        AccountService service = new AccountService();
        Account acountDetails = new Account(0);
        acountDetails.setBalance(BigDecimal.valueOf(50));
        Account sourceAccount = service.createAccount(acountDetails);
        Account targetAccount = service.createAccount();
        Transfer transfer = new Transfer(new Account(targetAccount.getId()), BigDecimal.valueOf(100));
        assertThrows(NotEnoughMoneyException.class, () -> service.transfer(sourceAccount, transfer));
        Account sourceAccountAfter = service.getAccount(new Account(sourceAccount.getId()));
        Account targetAccountAfter = service.getAccount(new Account(targetAccount.getId()));
        assertEquals(BigDecimal.valueOf(50), sourceAccountAfter.getBalance());
        assertEquals(BigDecimal.valueOf(0), targetAccountAfter.getBalance());
    }
}