Money transfer backend application.

Usage:
./mvnw clean package - runs tests and builds jar files:
   1. ./target/moneytransfer-1.0.jar  - jar without dependencies
   2. ./target/moneytransfer-1.0-jar-with-dependencies.jar - jar with all dependencies

java -jar target/moneytransfer-1.0-jar-with-dependencies.jar [server_port] - starts the applicatin on provided port.
If server_port is not set, the application is running on port 9080

Prebuilt .jaf files are located in a folder ./dist:
  ./dist/moneytransfer-1.0.jar
  ./dist/moneytransfer-1.0-jar-with-dependencies.jar

The following services are provided:
POST /accounts - create an account. Account details can be provided in request body, e.g.
  {"balance":500}

GET /accounts/ - get existing accounts
GET /accounts/<id> - get account details by id
POST /accounts/<id>/transfers/ - make a transfer from account with <id>. Target account and money are provided
  in request body, e.g.:
  { 
     "targetAccount":{ 
        "id":3
     },
     "moneyToTransfer":100000
  }
